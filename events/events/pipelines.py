# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
# useful for handling different item types with a single interface

from itemadapter import ItemAdapter
import pymongo
import logging

class EventsPipeline:
    collection_name ="data"
    
    def open_spider(self,spider):

        self.client = pymongo.MongoClient("mongodb+srv://faheem:mongodb@cluster0.lub2b.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
        self.db = self.client["IMDB"]
    
    def close_spider(self,spider):
        self.client.close()

    def process_item(self, item, spider):
        # self.db[self.collection_name].insert(item)
    
        dup_check = self.db[self.collection_name].find({'hashed-data':item['hashed-data']}).count()
        if dup_check == 0 :     
            # self.db[self.collection_name].insert(dict(item))
            self.db[self.collection_name].insert(item)
            logging.debug("site not updated!")
            logging.info("not updated")
        else:
            # self.db[self.collection_name].insert(item)
            logging.debug("site updated!")
            logging.info("updated")
        return item

